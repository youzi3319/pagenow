import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router);

/**
 * 白名单，此名单下的路由无需判断用户是否登录
 * @type {string[]}
 */
const whitelist = [
  '/login'
];

/**
 * 黑名单，此名单下的路由需要做登录校验
 * @type {string[]}
 */
const blacklist = [
  '/',
  '/admin',
  '/admin/project_manage',
  '/admin/page_template_manage',
  '/admin/enshrine_comp_manage',
  '/admin/compinfo_manage',
  '/admin/echart_theme_manage',
  '/admin/database_manage',
  '/admin/csv_datasource_manage',
  '/admin/resource_manage',
  '/admin/user_manage',
  '/admin/attachment_manage',
  '/admin/mapGeoJson_manage',
  '/admin/log_login_manage',
  '/admin/log_operate_manage',
  '/designer',
];

const router = new Router({
  mode: 'history', // 此路由模式不能修改，必须使用history模式
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: () => import('./views/Login.vue')
    },
    {
      path: '/',
      redirect: '/admin/project_manage'
      // name: 'home',
      // component: resolve => require(['@/views/Home'], resolve)
    },
    {
      path: '/admin',
      name: 'Admin',
      component: () => import('./views/Admin.vue'),
      children: [
        {
          path: 'project_manage',
          name: 'ProjectManage',
          component: () => import('./views/components/admin/project/ProjectManage.vue')
        },
        {
          path: 'page_template_manage',
          name: 'PageTemplateManage',
          component: () => import('./views/components/admin/pageTemplate/PageTemplateManage')
        },
        {
          path: 'enshrine_comp_manage',
          name: 'EnshrineCompManage',
          component: () => import('./views/components/admin/enshrineComp/EnshrineCompManage')
        },
        {
          path: 'compinfo_manage',
          name: 'CompinfoManage',
          component: () => import('./views/components/admin/compinfo/CompinfoManage.vue')
        },
        {
          path: 'echart_theme_manage',
          name: 'EchartThemeManage',
          component: () => import('./views/components/admin/echartTheme/EchartThemeManage.vue')
        },
        {
          path: 'database_manage',
          name: 'DatabaseManage',
          component: () => import('./views/components/admin/database/DatabaseManage.vue')
        },
        {
          path: 'csv_datasource_manage',
          name: 'CsvDatasourceManage',
          component: () => import('./views/components/admin/csvDatasource/CsvDatasourceManage.vue')
        },
        {
          path: 'resource_manage',
          name: 'ResourceManage',
          component: () => import('./views/components/admin/resource/ResourceManage.vue')
        },
        {
          path: 'user_manage',
          name: 'UserManage',
          component: () => import('./views/components/admin/user/UserManage.vue')
        },
        {
          path: 'attachment_manage',
          name: 'AttachmentManage',
          component: () => import('./views/components/admin/attachment/AttachmentManage.vue')
        },
        {
          path: 'mapGeoJson_manage',
          name: 'MapGeoJsonManage',
          component: () => import('./views/components/admin/mapGeoJson/MapGeoJsonManage.vue')
        },
        {
          path: 'log_login_manage',
          name: 'LogLoginManage',
          component: () => import('./views/components/admin/log/LogLoginManage.vue')
        },
        {
          path: 'log_operate_manage',
          name: 'LogOperateManage',
          component: () => import('./views/components/admin/log/LogOperateManage.vue')
        },
        {
          path: 'change-log',
          name: 'ChangeLog',
          component: () => import('./views/ChangeLog')
        }
      ],
    },
    {
      path: '/designer',
      name: 'Designer',
      meta: {
        title: 'PageNow - 设计器'
      },
      component: () => import('./views/Designer.vue')
    }
  ]
});

router.beforeEach((to, from, next) => {
  if (to.meta.title) {
    document.title = to.meta.title
  }
  let token = localStorage.getItem('token');
  if (token) {
    next()
  }else {
    if (!whitelist.includes(to.path) && !blacklist.includes(to.path)) { // 即不在白名单也不在黑名单中，即访问的是发布的页面
      next()
    }
    if (whitelist.includes(to.path)) {
      next()
    }else if (blacklist.includes(to.path)) {
      next('/login')
    }
  }
  /*if (whitelist.includes(to.path)) {
    next()
  }else {
    if (blacklist.includes(to.path)) {
      let token = localStorage.getItem('token');
      if (token === null || token === '' || token === 'null') {
        next('/login')
      }
    }
    next()
  }

  if (to.meta.title) {
    document.title = to.meta.title
  }

  next();*/
});


export default router
