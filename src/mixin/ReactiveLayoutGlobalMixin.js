
const ReactiveLayoutGlobalMixin = {
  data() {
    return {
      gridLayoutItems: [],
      hiddenGridLayoutItems: [],

      keydownFlag: false, // 用此变量来限制keydown事件在windows系统下无限触发的问题
    }
  },
  provide() {
    return {
      setChildrenRef: (componentId, ref) => {
        this[componentId] = ref
      },
      getChildrenRef: (componentId) => {
        if (this[componentId]) {
          return this[componentId]
        }else {
          this.$Message.error({
            content: '函数findCompVmById找不到指定ID【'+componentId+'】的组件实例',
            duration: 4
          })
        }
      },
      getThisRef: () => {
        return this
      }
    }
  },
  created () {

  },
  methods: {

    /**
     * 构建响应式栅格布局绑定的的数据
     */
    buildGridLayoutItems () {
      this.gridLayoutItems = [];
      let tmpLayoutItems = Object.assign([], this.layout.layoutItems);
      tmpLayoutItems.forEach(item => {
        let obj = {
          i: item.id,
          x: item.layoutItemConfigData.x,
          y: item.layoutItemConfigData.y,
          w: item.layoutItemConfigData.w,
          h: item.layoutItemConfigData.h,
          static: item.layoutItemConfigData.isStatic,

          id: item.id,
          name: item.name,
          aliasName: item.aliasName,
          layoutItemConfigData: item.layoutItemConfigData,
          component: item.component
        };
        if (item.layoutItemConfigData.display == 'block') {
          this.gridLayoutItems.push(obj)
        }else if (item.layoutItemConfigData.display == 'none') {
          this.hiddenGridLayoutItems.push(obj)
        }
      });
    },

    /**
     * 注册监听键盘按键
     */
    registerKeyDownAndUp () {
      let _this = this;

      $(document).unbind('keydown');
      $(document).unbind('keyup');

      this.registerKeyCtrlAndS();

      $(document).bind("keydown", function(e) {
        let keyCode = event.keyCode || event.which || event.charCode;
        let ctrlKey = event.ctrlKey || event.metaKey;

        // 快捷删除，mac下为cmd+backspace，windows下为delete键
        if ((ctrlKey && keyCode == 8) || keyCode == 46) {
          event.preventDefault();
          _this.$DesignerCommonUtil.backspaceDeleteLayoutOrGroupItem(
            _this.currentSelectLayoutItemId, _this.currentSelectLayoutItemIds, _this.currentSelectGroupItemId, _this.currentSelectGroupItemIds)
        }

        // ctrl + z 回退、撤销操作
        if (ctrlKey && keyCode == 90) {
          event.preventDefault();
          _this.$store.dispatch('designer/stepBackward');
        }

        // ctrl + y 单步恢复
        if (ctrlKey && keyCode == 89) {
          event.preventDefault();
          _this.$store.dispatch('designer/stepRecover');
        }

        // 解决windows系统下，keydown一直触发的问题
        if (_this.keydownFlag) {
          return
        }
        _this.keydownFlag = true;

        if(_this.$PnUtil.isMac()) {
          if(ctrlKey) { // Command键
            _this.$store.commit('designer/setKeepCtrl', true)
            if(_this.tmpCurrentSelectLayoutItemId) {
              let tmpIds = _this.currentSelectLayoutItemIds.concat();
              tmpIds.pushNoRepeat(_this.tmpCurrentSelectLayoutItemId);
              _this.$store.commit('designer/setCurrentSelectLayoutItemIds', tmpIds)
            }
          }
        }else if(_this.$PnUtil.isWindows()) {
          if(ctrlKey) { // Ctrl键
            _this.$store.commit('designer/setKeepCtrl', true)
            if(_this.tmpCurrentSelectLayoutItemId) {
              let tmpIds = _this.currentSelectLayoutItemIds.concat();
              tmpIds.pushNoRepeat(_this.tmpCurrentSelectLayoutItemId);
              _this.$store.commit('designer/setCurrentSelectLayoutItemIds', tmpIds)
            }
          }
        }

      })
      $(document).bind("keyup", function(e) {
        _this.keydownFlag = false;
        if(_this.$PnUtil.isMac()) {
          if(e.keyCode == 91) { // Command键
            _this.$store.commit('designer/setKeepCtrl', false)
          }
          if (e.keyCode == 93) {
            _this.$store.commit('designer/setKeepCtrl', false)
          }
        }else if(_this.$PnUtil.isWindows()) {
          if(e.keyCode == 17) { // Ctrl键
            _this.$store.commit('designer/setKeepCtrl', false)
          }
        }
      });
    },

    /**
     * 构建布局块入场动画样式Class
     * @param layoutItem
     * @returns {string}
     */
    buildEnterActiveClass (layoutItem) {
      if(this.$store.state.release.pageMetadata) {
        if(layoutItem.layoutItemConfigData.animationVisible) {
          return 'animated ' + layoutItem.layoutItemConfigData.inAnimation + ' ' + layoutItem.layoutItemConfigData.animationDelay;
        }else {
          return ''
        }
      }
    },

    /**
     * 构建布局块离场动画样式Class
     * @param layoutItem
     * @returns {string}
     */
    buildLeaveActiveClass (layoutItem) {
      if(this.$store.state.release.pageMetadata) {
        if(layoutItem.layoutItemConfigData.animationVisible) {
          return 'animated ' + layoutItem.layoutItemConfigData.outAnimation + ' ' + layoutItem.layoutItemConfigData.animationDelay;
        }else {
          return ''
        }
      }
    },

    /**
     * 构建用户自定义的class名称
     * @param layoutItem
     * @returns {string}
     */
    buildLayoutItemCustomClassName (layoutItem) {
      let clazz = '';
      if (layoutItem.layoutItemConfigData.className) {
        clazz = ' ' + layoutItem.layoutItemConfigData.className
      }
      return clazz
    },

    /**
     * 构建3d旋转样式
     * @param layoutItem
     * @returns {string}
     */
    buildLayoutItem3dTransformStyleValue (layoutItem) {
      let style = 'translate3d(0px, 0px, 0px) scale3d(1, 1, 1)';
      if (layoutItem.layoutItemConfigData.use3dRotate) {
        if (layoutItem.layoutItemConfigData.axisOfRotation == 'x') {
          style += ' rotate3d(1, 0, 0, ' + layoutItem.layoutItemConfigData.rotationAngleFor3d + 'deg)'
        }else if (layoutItem.layoutItemConfigData.axisOfRotation == 'y') {
          style += ' rotate3d(0, 1, 0, ' + layoutItem.layoutItemConfigData.rotationAngleFor3d + 'deg)'
        }
      }else {
        style = 'none'
      }
      return style
    },

    /**
     * 构建滤镜样式
     * @param layoutConfigData
     * @returns {string}
     */
    buildLayoutItemStyleFilter (layoutConfigData) {
      let clazz = '';
      if (layoutConfigData.styleFilter_use) {
        clazz =
          ' hue-rotate(' + layoutConfigData.styleFilter_hueRotate + 'deg)' +
          ' contrast(' + layoutConfigData.styleFilter_contrast + '%)' +
          ' opacity(' + layoutConfigData.styleFilter_opacity + '%)' +
          ' saturate(' + layoutConfigData.styleFilter_saturate + '%)' +
          ' brightness(' + layoutConfigData.styleFilter_brightness + '%)';
      }
      return clazz
    },

    layoutItemBackgroundImageSrc (layoutItem) {
      return window.g.AXIOS_BASE_URL + layoutItem.layoutItemConfigData.imageRelativePath
    }
  },
  computed: {

  },
  asyncComputed: {
    canvasBackgroundImageSrc () {
      return window.g.AXIOS_BASE_URL + this.layout.layoutConfigData.imageRelativePath
    }
  }
};

export default ReactiveLayoutGlobalMixin
