
import { createHelpers } from 'vuex-map-fields';

const { mapFields } = createHelpers({
  getterType: 'designer/getField',
  mutationType: 'designer/updateField',
});

const CanvasDesignerMixin = {
  data() {
    return {

    }
  },
  created () {


  },
  methods: {

    /**
     * 注册浏览器关闭监听
     */
    registerWindowBeforeunload () {
      let _this = this;
      window.onbeforeunload = function (e) {
        e = e || window.event;
        // 兼容IE8和Firefox 4之前的版本
        if (e) {
          e.returnValue = '系统可能不会保存您所做的更改。';
        }
        // Chrome, Safari, Firefox 4+, Opera 12+ , IE 9+
        return '系统可能不会保存您所做的更改。';
      };
    },

    /**
     * 注册键盘ctrl + s组合键监听
     */
    registerKeyCtrlAndS () {
      let _this = this;
      $(document).unbind('keydown');
      $(document).bind("keydown", function(e) {
        let keyCode = event.keyCode || event.which || event.charCode;
        let ctrlKey = event.ctrlKey || event.metaKey;
        if(ctrlKey && keyCode == 83) {
          event.preventDefault();
          if (_this.$store.state.designer.isAlongCompEditModel) {
            _this.$Message.info('当前为独立组件编辑模式，无法保存，请先退出此模式')
          }else {
            _this.$DesignerCommonUtil.saveCurrentEditPage(_this.pageMetadata)
          }
        }
      });
    },

    /**
     * 画布缩放
     * @param operation
     */
    canvasZoomChange (operation) {
      switch (operation) {
        case 'zoomIn':
          // if (this.zoomValue + 0.2 <= 2) {
          //   this.$store.commit('designer/setZoomValue', this.zoomValue += 0.2)
          // }
          if (this.zoomValue + 0.2 > 1) {
            this.$store.commit('designer/setZoomValue', 1)
          }else {
            this.$store.commit('designer/setZoomValue', this.zoomValue += 0.2)
          }
          break
        case 'zoomOut':
          if (this.zoomValue - 0.2 >= 0.3) {
            this.$store.commit('designer/setZoomValue', this.zoomValue -= 0.2)
          }
          break
        case 'zoomAuto':
          this.$DesignerCommonUtil.initDesignerZoomValue(this.pageMetadata.layout.layoutConfigData.width)
          break
        case 'zoomReset':
          this.$store.commit('designer/setZoomValue', 1)
          break
      }
    },

    buildLayoutItemActiveClass (layoutItem) {
      if (this.currentSelectLayoutItemIds.length > 0) {
        if (this.currentSelectLayoutItemIds.indexOf(layoutItem.id) > -1) {
          return 'active'
        }else {
          return ''
        }
      }else {
        if (this.currentSelectLayoutItemId == layoutItem.id) {
          return 'active'
        }
      }
      if (this.currentHoverLayoutItemId == layoutItem.id) {
        return 'active'
      }
      return ''
    }


  },
  computed: {
    ...mapFields({
      pageMetadata: 'pageMetadata',
      currentSelectLayoutItemId: 'currentSelectLayoutItemId',
      currentSelectLayoutItemIds: 'currentSelectLayoutItemIds',
      currentHoverLayoutItemId: 'currentHoverLayoutItemId',
      zoomValue: 'zoomValue'
    })
  }
};

export default CanvasDesignerMixin
