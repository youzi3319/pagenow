/**
 * 管理界面混入对象，内部定义了一些分页信息和通用表格的数据对象等...
 */
const ManageMixin = {
  data() {
    return {
      pageIndex: 1,
      pageSize: 12,
      pageTotal: 0,
      tableData: []
    }
  },
  methods: {
    /**
     * 表单对象赋值:
     * 对目标对象存在且源对象同样存在的属性，全部覆盖；
     * 目标对象不存在但是源对象存在的属性， 全部丢弃；
     * 目标对象存在但是源对象不存在的属性，如果是字符串赋值为空串，其余类型赋值为undefined
     * @param target
     * @param source
     * @returns {any}
     */
    recover (target, source) {
      if (target === undefined || target === null) { throw new TypeError('Cannot convert first argument to object') }
      let to = Object(target)
      if (source === undefined || source === null) { return to }
      let keysArray = Object.keys(Object(target))
      for (let nextIndex = 0, len = keysArray.length; nextIndex < len; nextIndex++) {
        let nextKey = keysArray[nextIndex]
        let desc = Object.getOwnPropertyDescriptor(target, nextKey)
        if (desc !== undefined && desc.enumerable) {
          if (to.hasOwnProperty(nextKey)) {
            if (to[nextKey] instanceof Array) {
              to[nextKey] = source[nextKey]
            } else if (to[nextKey] instanceof Object) {
              this.recover(to[nextKey], source[nextKey])
            } else if (source[nextKey] !== undefined) {
              to[nextKey] = source[nextKey]
            } else if (typeof (to[nextKey]) === 'string') {
              to[nextKey] = ''
            } else {
              to[nextKey] = undefined
            }
          }
        }
      }
      return to
    }
  },
  computed: {

  }
};

export default ManageMixin
