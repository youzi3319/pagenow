const FormWidgetMixin = {
  props: {
    // 传入布局块根节点配置数据
    layoutItem: {
      type: Object,
      required: true
    },
    formData: {
      type: Object,
      default() {
        return {}
      }
    }
  },
  data() {
    return {
      component: this.layoutItem.component,
      runCustomGlobalJsCodeFlag: -1, // 标识是否可也运行全局脚本
    }
  },
  mounted() {

  },
  destroyed () {

  },
  methods: {

    /**
     * 运行表单部件自定义编辑的onMounted交互事件代码
     */
    runWidgetOnMountedEventCode() {
      if(this.$store.state.release.pageMetadata) {
        if (this.component.compConfigData.widgetEventCodeConfig.onMounted) {
          if (this.runCustomGlobalJsCodeFlag == -1) {
            try {
              eval(this.component.compConfigData.widgetEventCodeConfig.onMounted.code);
              this.runCustomGlobalJsCodeFlag = 1
            }catch (e) {
               this.$Message.error({
                 content: '组件交互事件【onMounted】回调脚本运行异常，异常信息：' + e.message,
                 duration: 5
               })
            }
          }
        }
      }
    },

    /**
     *
     * @param eventName
     * @param value
     */
    runWidgetEventCode(eventName, _callback) {
      if(this.$store.state.release.pageMetadata) {
        if (this.component.compConfigData.widgetEventCodeConfig[eventName]) {
          let fn = new Function(this.component.compConfigData.widgetEventCodeConfig[eventName].resultValues, this.component.compConfigData.widgetEventCodeConfig[eventName].code)
          try{
            if(_callback) {
              _callback(fn)
            }else {
              fn.call(this) 
            }
          }catch (e) {
            this.$Message.error({
              content: '组件交互事件【'+eventName+'】回调脚本运行异常，异常信息：' + e.message,
              duration: 5
            })
          }
        }
      }
    }
  },
  computed: {
    /**
     * 获取表单Form的VM实例
     * @returns {null|Vue|Element|Vue[]|Element[]}
     */
    pnForm() {
      if(this.$store.state.release.pageMetadata) {
        return this.$root.$children[0].$children[0].$refs['pnForm']
      }
      return null
    },

    isDesigner() {
      if(this.$store.state.release.pageMetadata) {
        return false
      }
      return true
    }
  }
}

export default FormWidgetMixin
