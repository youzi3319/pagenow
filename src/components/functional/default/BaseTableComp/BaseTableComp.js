
const buildProgressObj = function () {
  return {
    striped: false,
    type: 'line',
    strokeWidth: 14,
    strokeColor: '#42a5f5',
    trackColor: '#bbdefb',
    textInside: true,
    status: '',
    width: 40,
    showText: true,
    textColor: '#fff',
    textFontSize: 12,
    reverse: false
  }
};

const buildTrendObj = function () {
  return {
    showIcon: true,
    icon: '',
    iconSize: 13,
    topColor: '#4caf50',
    equalColor: '#ff9800',
    downColor: '#f44336',
    syncTextColor: false,
    baseValue: 0, // 基础值
    suffixText: '' // 后缀
  }
};

export default {
  buildProgressObj,
  buildTrendObj
}
