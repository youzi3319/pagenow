import PnDesigner from "@/utils/PnDesigner";
import PnUtil from "@/utils/PnUtil";

const buildSeriesObj = function (name, type, otherConfig = {}) {
  return PnUtil.deepMerge({
    name: name,
    type: type,
    yAxisIndex: 0,
    showBackground: false,
    backgroundStyle: {
      color: 'rgba(180, 180, 180, 0.2)',
      borderColor: '#424242',
      borderWidth: 0,
      borderType: 'solid',
      borderRadius: 0,
      shadowBlur: 0,
      shadowColor: '#424242'
    },
    label: {
      show: false,
      position: 'inside',
      rotate: 0,
      formatter: '{c}',
      color: '#fff',
      fontSize: 12
    },
    itemStyle: {
      color: '',
      startColor: '',
      endColor: '',
      borderColor: '',
      borderWidth: 0,
      borderType: 'solid',
      barBorderRadius: 0,
      shadowBlur: 0,
      shadowColor: '#424242'
    },

    barWidth: '',
    barGap: '20%', // 不同系列的柱间距离
    barCategoryGap: '20%', // 同一系列的柱间距离

    symbolSize: 5,
    smooth: false,
    lineStyle: {
      type: 'solid',
      width: 2
    },

    markLine: PnDesigner.buildMarkLineConfigData(),
    markPoint: PnDesigner.buildMarkPointConfigData()
  }, otherConfig)
};

const buildDefaultSeriesObj = function (name = '', data) {
  return {
    name: name,
    type: 'bar',
    yAxisIndex: 0,
    showBackground: false,
    backgroundStyle: {
      color: 'rgba(180, 180, 180, 0.2)',
      borderColor: '#424242',
      borderWidth: 0,
      borderType: 'solid',
      borderRadius: 0,
      shadowBlur: 0,
      shadowColor: '#424242'
    },
    label: {
      show: false,
      position: 'inside',
      rotate: 0,
      formatter: '{c}',
      color: '#fff',
      fontSize: 12
    },
    itemStyle: {
      color: '',
      startColor: '',
      endColor: '',
      borderColor: '',
      borderWidth: 0,
      borderType: 'solid',
      barBorderRadius: 0,
      shadowBlur: 0,
      shadowColor: '#424242'
    },

    barWidth: '',
    barGap: '20%', // 不同系列的柱间距离
    barCategoryGap: '20%', // 同一系列的柱间距离

    symbolSize: 5,
    smooth: false,
    lineStyle: {
      type: 'solid',
      width: 2
    },
    data: data
  }
}

export default {
  buildSeriesObj,
  buildDefaultSeriesObj
}
