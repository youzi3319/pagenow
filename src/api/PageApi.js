import {Axios} from '../utils/AxiosPlugin'

const savePage = async function (savePnPageForm, pageTemplateId = '') {
  savePnPageForm.pageTemplateId = pageTemplateId
  return await Axios.post('/page/savePage', savePnPageForm);
};

const getPagesByProjectId = async function (project_id) {
  return await Axios.get('/page/getPagesByProjectId', {params: {project_id: project_id}});
};

const updatePage = async function (page) {
  return await Axios.post('/page/updatePage', page);
};

const deletePage = async function (id) {
  return await Axios.delete('/page/deletePage', {params: {id: id}});
};

const getPageById = async function (id) {
  return await Axios.get('/page/getPageById', {params: {id: id}});
};

const getAllPage = async function (pageParam) {
  return await Axios.post('/page/getAllPage', pageParam);
};

const releasePage = async function (pageId, publish, encrypt, password) {
  return await Axios.post('/page/releasePage', {pageId: pageId, publish: publish, encrypt: encrypt, password: password});
};

const copyPage = async function (project_id, page_id) {
  return await Axios.post('/page/copyPage', {project_id: project_id, page_id: page_id});
};

export default {
  savePage,
  getPagesByProjectId,
  updatePage,
  deletePage,
  getPageById,
  getAllPage,
  releasePage,
  copyPage
}
