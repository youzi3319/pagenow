import {Axios} from '../utils/AxiosPlugin'

const savePageTemplate = async function (pageTemplate) {
  return await Axios.post('/pageTemplate/savePageTemplate', pageTemplate);
};

const getPageTemplateByPage = async function (pageIndex, pageSize, name) {
  return await Axios.post('/pageTemplate/getPageTemplateByPage', {pageIndex: pageIndex, pageSize: pageSize, name: name});
};

const getAllPageTemplate = async function () {
  return await Axios.get('/pageTemplate/getAllPageTemplate');
};

const getPageTemplateById = async function (id) {
  return await Axios.get('/pageTemplate/getPageTemplateById', {params: {id: id}});
};

const toggleEnabled = async function (id) {
  return await Axios.post('/pageTemplate/toggleEnabled', {id: id});
};

const deletePageTemplate = async function (id) {
  return await Axios.delete('/pageTemplate/deletePageTemplate', {params: {id: id}});
};

const getPageTemplateLayout = async function (id) {
  return await Axios.get('/pageTemplate/getPageTemplateLayout', {params: {id: id}});
};

const getPageTemplateThemeJson = async function (id) {
  return await Axios.get('/pageTemplate/getPageTemplateThemeJson', {params: {id: id}});
};

const getPageTemplateForCreatePageForm = async function (pageIndex, pageSize, is_sys) {
  return await Axios.post('/pageTemplate/getPageTemplateForCreatePageForm', {pageIndex: pageIndex, pageSize: pageSize, is_sys: is_sys});
};

const attachmentResourceTransfer = async function (oldGuid, newGuid) {
  return await Axios.post('/pageTemplate/attachmentResourceTransfer', {oldGuid: oldGuid, newGuid: newGuid});
};

export default {
  savePageTemplate,
  getPageTemplateByPage,
  getAllPageTemplate,
  getPageTemplateById,
  toggleEnabled,
  deletePageTemplate,
  getPageTemplateLayout,
  getPageTemplateThemeJson,
  getPageTemplateForCreatePageForm,
  attachmentResourceTransfer
}
