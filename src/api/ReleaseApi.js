import {Axios} from '../utils/AxiosPlugin'

const getReleaseData = async function (pageId) {
  return await Axios.get('/release/getReleaseData', {params: {pageId: pageId}});
};

const loadDbRoutes = async function () {
  return await Axios.get('/release/loadDbRoutes');
};

export default {
  getReleaseData,
  loadDbRoutes
}
