import Vue from 'vue'
import Vuex from 'vuex'

import common from './modules/common'
import designer from './modules/designer'
import release from './modules/release'
import manage from './modules/manage'
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
  modules: {
    common: common,
    designer: designer,
    release: release,
    manage: manage
  },
  strict: debug,
  plugins: [createPersistedState({
    reducer (val) {
      return {
        designer: {
          structureTreeSidebarCollapsed: val.designer.structureTreeSidebarCollapsed,
          componentLibrarySidebarCollapsed: val.designer.componentLibrarySidebarCollapsed,
          rightSidebarCollapsed: val.designer.rightSidebarCollapsed,
          canvasMapCollapsed: val.designer.canvasMapCollapsed,
          pageLinesData: val.designer.pageLinesData,
          zoomValue: val.designer.zoomValue,
          purenessLayoutEditModel: val.designer.purenessLayoutEditModel,
          autoSavePage: val.designer.autoSavePage,
          autoSaveInterval: val.designer.autoSaveInterval,
          rulerGuidesMagnetThreshold: val.designer.rulerGuidesMagnetThreshold,
          dragAlignMagnetThreshold: val.designer.dragAlignMagnetThreshold,
          adaptionDesignerCanvasZoom: val.designer.adaptionDesignerCanvasZoom
        },
        manage: {

        }
      }
    }
  })]
})
