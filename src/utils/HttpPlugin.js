import axios from 'axios'
import { LoadingBar } from 'view-design'

// 创建 axios 实例
// 这里 export 的原因是方便组件外使用 axios
export const Http = axios.create({
  timeout: 100000,
});

// POST传参序列化(添加请求拦截器)
// 在发送请求之前处理相关逻辑
Http.interceptors.request.use(config => {
  LoadingBar.start();

  // 设置以 form 表单的形式提交参数，如果以 JSON 的形式提交表单，可忽略
  if(config.method  === 'post' && config.headers['Content-Type'] === 'application/x-www-form-urlencoded'){

    let ret = '';
    for (let it in config.data) {
      ret += encodeURIComponent(it) + '=' + encodeURIComponent(config.data[it]) + '&'
    }
    config.data = ret;

  }

  return config

},error =>{
  return Promise.reject(error)
});

// 返回状态判断(添加响应拦截器)
Http.interceptors.response.use(res => {
  LoadingBar.finish();

  return res
}, error => {
  LoadingBar.error();

  // 返回 response 里的错误信息
  return Promise.reject(error.response.data)
});

// 将 Axios 实例添加到Vue的原型对象上
export default {
  install(Vue) {
    Object.defineProperty(Vue.prototype, '$axios', { value: Http })
  }
}

