(function webpackUniversalModuleDefinition(root, factory) {
  if (typeof exports === 'object' && typeof module === 'object')
    module.exports = factory();
  else if (typeof define === 'function' && define.amd)
    define("canvasmap", [], factory);
  else if (typeof exports === 'object')
    exports["canvasmap"] = factory();
  else
    root["canvasmap"] = factory();
})((typeof self !== 'undefined' ? self : this), function () {
  return (function (modules) { // webpackBootstrap
    let installedModules = {};

    function __webpack_require__(moduleId) {
      if (installedModules[moduleId]) {
        return installedModules[moduleId].exports;
      }
      let module = installedModules[moduleId] = {
        i: moduleId,
        l: false,
        exports: {}
      };
      modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
      module.l = true;
      return module.exports;
    }

    __webpack_require__.m = modules;
    __webpack_require__.c = installedModules;
    __webpack_require__.d = function (exports, name, getter) {
      if (!__webpack_require__.o(exports, name)) {
        Object.defineProperty(exports, name, {enumerable: true, get: getter});
      }
    };
    __webpack_require__.r = function (exports) {
      if (typeof Symbol !== 'undefined' && Symbol.toStringTag) {
        Object.defineProperty(exports, Symbol.toStringTag, {value: 'Module'});
      }
      Object.defineProperty(exports, '__esModule', {value: true});
    };
    __webpack_require__.t = function (value, mode) {
      if (mode & 1) value = __webpack_require__(value);
      if (mode & 8) return value;
      if ((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
      let ns = Object.create(null);
      __webpack_require__.r(ns);
      Object.defineProperty(ns, 'default', {enumerable: true, value: value});
      if (mode & 2 && typeof value != 'string') for (let key in value) __webpack_require__.d(ns, key, function (key) {
        return value[key];
      }.bind(null, key));
      return ns;
    };
    __webpack_require__.n = function (module) {
      let getter = module && module.__esModule ?
        function getDefault() {
          return module['default'];
        } :
        function getModuleExports() {
          return module;
        };
      __webpack_require__.d(getter, 'a', getter);
      return getter;
    };
    __webpack_require__.o = function (object, property) {
      return Object.prototype.hasOwnProperty.call(object, property);
    };
    __webpack_require__.p = "";
    return __webpack_require__(__webpack_require__.s = 0);
  })
    // eslint-disable-next-line no-unexpected-multiline
    ([
      (function (module, exports, __webpack_require__) {

        (function (global) {
          module.exports = function (canvas, options) {
            let WIN = global.window;
            let DOC = WIN.document;
            let DOC_EL = DOC.documentElement;
            let BODY = DOC.querySelector('body');
            let CTX = canvas.getContext('2d');

            let black = function black(pc) {
              return "rgba(0,0,0,".concat(pc / 100, ")");
            };

            let settings = Object.assign({
              viewport: null,
              styles: {
                'header,footer,section,article': black(8),
                'h1,a': black(10),
                'h2,h3,h4': black(8)
              },
              back: black(2),
              view: black(5),
              drag: black(10),
              interval: null
            }, options);

            let _listener = function _listener(el, method, types, fn) {
              return types.split(/\s+/).forEach(function (type) {
                return el[method](type, fn);
              });
            };

            let on = function on(el, types, fn) {
              return _listener(el, 'addEventListener', types, fn);
            };

            let off = function off(el, types, fn) {
              return _listener(el, 'removeEventListener', types, fn);
            };

            let Rect = function Rect(x, y, w, h) {
              return {
                x: x,
                y: y,
                w: w,
                h: h
              };
            };

            let rect_rel_to = function rect_rel_to(rect) {
              let pos = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {
                x: 0,
                y: 0
              };
              return Rect(rect.x - pos.x, rect.y - pos.y, rect.w, rect.h);
            };

            let rect_of_doc = function rect_of_doc() {
              return Rect(0, 0, DOC_EL.scrollWidth, DOC_EL.scrollHeight);
            };

            let rect_of_win = function rect_of_win() {
              return Rect(WIN.pageXOffset, WIN.pageYOffset, DOC_EL.clientWidth, DOC_EL.clientHeight);
            };

            let el_get_offset = function el_get_offset(el) {
              let br = el.getBoundingClientRect();
              return {
                x: br.left + WIN.pageXOffset,
                y: br.top + WIN.pageYOffset
              };
            };

            let rect_of_el = function rect_of_el(el) {
              let _el_get_offset = el_get_offset(el),
                x = _el_get_offset.x,
                y = _el_get_offset.y;

              return Rect(x, y, el.offsetWidth, el.offsetHeight);
            };

            let rect_of_viewport = function rect_of_viewport(el) {
              let _el_get_offset2 = el_get_offset(el),
                x = _el_get_offset2.x,
                y = _el_get_offset2.y;

              return Rect(x + el.clientLeft, y + el.clientTop, el.clientWidth, el.clientHeight);
            };

            let rect_of_content = function rect_of_content(el) {
              let _el_get_offset3 = el_get_offset(el),
                x = _el_get_offset3.x,
                y = _el_get_offset3.y;

              return Rect(x + el.clientLeft - el.scrollLeft, y + el.clientTop - el.scrollTop, el.scrollWidth, el.scrollHeight);
            };

            let calc_scale = function () {
              let width = canvas.clientWidth;
              let height = canvas.clientHeight;
              return function (w, h) {
                return Math.min(width / w, height / h);
              };
            }();

            let resize_canvas = function resize_canvas(w, h) {
              canvas.width = w;
              canvas.height = h;
              canvas.style.width = "".concat(w, "px");
              canvas.style.height = "".concat(h, "px");
            };

            let viewport = settings.viewport;

            let find = function find(sel) {
              return Array.from((viewport || DOC).querySelectorAll(sel));
            };

            let drag = false;
            let root_rect;
            let view_rect;
            let scale;
            let drag_rx;
            let drag_ry;

            let draw_rect = function draw_rect(rect, col) {
              if (col) {
                CTX.beginPath();
                CTX.rect(rect.x, rect.y, rect.w, rect.h);
                CTX.fillStyle = col;
                CTX.fill();
              }
            };

            let apply_styles = function apply_styles(styles) {
              Object.keys(styles).forEach(function (sel) {
                let col = styles[sel];
                find(sel).forEach(function (el) {
                  draw_rect(rect_rel_to(rect_of_el(el), root_rect), col);
                });
              });
            };

            let draw = function draw() {
              root_rect = viewport ? rect_of_content(viewport) : rect_of_doc();
              view_rect = viewport ? rect_of_viewport(viewport) : rect_of_win();
              scale = calc_scale(root_rect.w, root_rect.h);
              resize_canvas(root_rect.w * scale, root_rect.h * scale);
              CTX.setTransform(1, 0, 0, 1, 0, 0);
              CTX.clearRect(0, 0, canvas.width, canvas.height);
              CTX.scale(scale, scale);
              draw_rect(rect_rel_to(root_rect, root_rect), settings.back);
              apply_styles(settings.styles);
              draw_rect(rect_rel_to(view_rect, root_rect), drag ? settings.drag : settings.view);
            };

            let on_drag = function on_drag(ev) {
              ev.preventDefault();
              let cr = rect_of_viewport(canvas);
              let x = (ev.pageX - cr.x) / scale - view_rect.w * drag_rx;
              let y = (ev.pageY - cr.y) / scale - view_rect.h * drag_ry;

              if (viewport) {
                viewport.scrollLeft = x;
                viewport.scrollTop = y;
              } else {
                WIN.scrollTo(x, y);
              }

              draw();
            };

            let on_drag_end = function on_drag_end(ev) {
              drag = false;
              canvas.style.cursor = 'pointer';
              BODY.style.cursor = 'auto';
              off(WIN, 'mousemove', on_drag);
              off(WIN, 'mouseup', on_drag_end);
              on_drag(ev);
            };

            let on_drag_start = function on_drag_start(ev) {
              drag = true;
              let cr = rect_of_viewport(canvas);
              let vr = rect_rel_to(view_rect, root_rect);
              drag_rx = ((ev.pageX - cr.x) / scale - vr.x) / vr.w;
              drag_ry = ((ev.pageY - cr.y) / scale - vr.y) / vr.h;

              if (drag_rx < 0 || drag_rx > 1 || drag_ry < 0 || drag_ry > 1) {
                drag_rx = 0.5;
                drag_ry = 0.5;
              }

              canvas.style.cursor = 'move';
              BODY.style.cursor = 'move';
              on(WIN, 'mousemove', on_drag);
              on(WIN, 'mouseup', on_drag_end);
              on_drag(ev);
            };

            let init = function init() {
              canvas.style.cursor = 'pointer';
              on(canvas, 'mousedown', on_drag_start);
              on(viewport || WIN, 'load resize scroll', draw);

              if (settings.interval > 0) {
                setInterval(function () {
                  return draw();
                }, settings.interval);
              }

              draw();
            };

            init();
            return {
              redraw: draw
            };
          };
        }.call(this, __webpack_require__(1)))

      }),
      (function (module, exports) {

        let g;

        g = (function () {
          return this;
        })();

        try {
          g = g || new Function("return this")();
        } catch (e) {
          if (typeof window === "object") g = window;
        }

        module.exports = g;

      })
    ]);
});