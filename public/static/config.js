/**
 * 与此文件统计目录下的config_prod.js文件用于配置生产环境下的相关信息，当运行yarn build打包项目时，会自动使用config_prod.js文件的内容覆盖config.js的内容。
 * 友情提醒：config_prod.js文件只是一个临时文件，用于配置项目在打包到生成环境之后的config信息，不论是开发环境或是生产环境，PageNow使用的始终都是config.js这个文件。
 */
function getRootPath () {
  return 'http://' + window.location.host.split(':')[0] + ':8090'
}

window.g = {
  /**
   * PROJECT_ROOT_NAME用于定义项目名称，此名称必须在开发环境下设置与vue.config.js配置文件中的publicPath属性保持一致
   * 备注：此变量在生产环境下【不可修改】
   */
  PROJECT_ROOT_NAME: '/pagenow',
  /**
   * 系统显示名称，此名称一般在系统运行时用于显示系统名称时使用，例如在管理界面左上角显示的名称
   * 备注：此变量在生产环境下【可修改】
   */
  SYS_NAME: 'PageNow',
  /**
   * 定义系统的显示版本号
   * 备注：此变量在生产环境下【可修改】
   */
  SYS_VERSION: '2.0',
  /**
   * axios基础路径，此路径一般与PageNow后端运行程序的IP和端口号对应。
   * 当进行项目打包的时候，建议先将此变量设置为生产环境下的后端服务地址，当然，在打包之后，也可以在打包后的目录中，在static文件夹下找到config.js配置文件进行修改
   * 备注：此变量在生产环境下【可修改】
   */
  AXIOS_BASE_URL: 'http://localhost:8090'
};
